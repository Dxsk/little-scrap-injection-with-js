from flask import Flask, request, Response
import requests
import shutil

app = Flask(__name__)

# Example /tmp
PATH = ''

@app.route('/', methods=['POST'])
def get_url_and_download():
    if request.method == 'POST':
        datas = request.data
        array_url = datas.decode('utf-8').split(',')
        print(array_url)
        for url in array_url:
            r = requests.get(url, stream = True)
            filename = url.split("/")[-1]

            if r.status_code == 200:
                r.raw.decode_content = True
                
                with open(f"{PATH}/{filename}",'wb') as f:
                    shutil.copyfileobj(r.raw, f)
                    
                print('Download sucessfull: ',filename)
            else:
                print('error')
        return 'ok'